
#!/bin/bash

if [ $# -eq 0 ]; then
    echo "INVALID PARAMETERS"
    exit 1
fi

if [ $# -ge 3 ]; then
    echo "INVALID PARAMETERS"
    exit 1
fi


if [ $1 -eq 1 ];then
    exit 2
fi

if [ $2 -eq 0 ]; then
    exit 2
fi    

FILA=$1
COLUMNA=$2
valor=$(sed -n "${FILA}p" aeropuertos_detalle.csv | cut -d ";" -f "$COLUMNA")
header=$(head -n1 aeropuertos_detalle.csv | cut -d ";" -f "$COLUMNA")
valores=$(cat aeropuertos_detalle.csv | cut -d ";" -f "$COLUMNA")

cant=0
for i in ${valores[@]}; do    
    if [[ "$valor" =~ "$i" ]]; then
        let cant++
    fi
done


echo $valor
echo $header
echo $cant
