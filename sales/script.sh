#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "INVALID PARAMETERS"
    exit 1
fi
if [[ -z "$1" ||  -z "$2" ]]
then 
    echo "INVALID PARAMETERS"
    exit 2
fi

if ! [[ "$1" =~ ^[a-zA-Z]+$ ||  "$2" =~ ^[a-zA-Z]+$ ]]
then
  echo "INVALID PARAMETERS"
  exit 3
fi
alt=$(cat 100_Sales_Records.csv | grep "$1" | grep $2 | cut -d, -f2,3,5,9 )
arr=()
suma=0
while read -r i;
do
    var=$(echo $i | grep "$1" | grep $2 | tr ' ' '-' | cut -d',' -f2)
    arr=( ${arr[@]} "$var" ) 
    var1=$(echo $i | grep "$1" | grep $2 | cut -d',' -f4);
    let "suma=suma + $var1" 2> /dev/null
done <<< "$alt"

for i in ${arr[@]}; do 
echo $i | tr '-' ' '
done
echo $suma

exit 0
