#!/bin/bash

if [ $# -ne 1 ]; then
  echo "INVALID PARAMETERS"
  exit 1
fi

if [[ ! $1 =~ ^[0-9]+$ ]]
then
  echo "INVALID PARAMETERS"
  exit 1
fi


sum_found=0
primer_coincidencia=""
ultima_coincidencia=""

for i in $(cat 2013_2020.csv); do

    n2=$(echo $i | cut -d ',' -f2)
    n3=$(echo $i | cut -d ',' -f3)
    n4=$(echo $i | cut -d ',' -f4)
    n5=$(echo $i | cut -d ',' -f5)
    n6=$(echo $i | cut -d ',' -f6)
    n7=$(echo $i | cut -d ',' -f7)

    fecha=$(echo $i | cut -d ',' -f1)

  #  echo "$n2+$n3+$n4+$n5+$n6+$n7"
    let "sum = 10#$n2 + 10#$n3 + 10#$n4 + 10#$n5 + 10#$n6 + 10#$n7"
   # echo $sum

    if [ $sum -eq $1 ];then
       sum_found=$((sum_found+1))

      if [ -z "$primer_coincidencia" ]
      then
        primer_coincidencia=$fecha
      fi
      ultima_coincidencia=$fecha

    fi

done

echo $sum_found
echo $primer_coincidencia
echo $ultima_coincidencia
