Se tiene un archivo con el resultado histórico de una Loteria, el archivo está delimitado por comas (archivo adjunto 2013_2020.csv).

Su script va a recibir un parámetro que debe ser un número y siempre mayor a 0.

El parámetro representa la suma de valores para la combinación ganadora. La combinación son los números de las columnas 2 a la 7 inclusive. Tenemos que encontrar la cantidad de sorteos en los que la suma de los números de la combinación ganadora coincide con el parámetro recibido.
Ademas debemos imprimir la fecha de la primer y ultima coincidencia que encontramos en el archivo(primera y última según el orden del archivo, no en orden temporal)-

Por ejemplo si recibo
180

Debo Imprimir:

3
24/10/2020
1/02/2018

Explicación: Hay 6 combinaciones que sumados sus números dan 180, la primera
del archivo corresponde al 24/10/2020 y la última al 18/10/2014.

Otro Ejemplo:

Si recibo:
370
Debo Imprimir
0
Explicación: No hay ninguna combinación ganadora cuta suma de números nos de 0

Si recibo: X
Debo Imprimir:
INVALID PARAMETERS

Explicación: El script debe recibir solo números

Es imperativo y OBLIGATORIO que su script retorne los valores en el orden indicado y como se detallo anteriormente.

No imprima cosas innecesarias por pantalla. En tal caso el resultado se considera inválido.
