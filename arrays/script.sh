#!/bin/bash
if [ $# -lt 1 ] ; then
    echo 'INVALID PARAMETERS'
    exit 1
fi 

arreglo=( "$@" )

function existe {
    num=0
    for i in ${!arreglo[*]}; do
        num=${arreglo[$i]}
        if [ $num -eq $1 ]; then 
            echo $i
            return 0
        fi
    done
    return 1     
}

function replace {
    if  existe $1 ; then
        i=$( existe $1 )
        arreglo[$i]=$2
        return 0
    fi
    return 1
}

function eliminar {
    if  existe $1 ; then
        i=$( existe $1 )
        unset arreglo[$i]
        arreglo=( ${arreglo[*]} )
        return 0
    fi
    return 1
}

function cantidad {
    echo ${#arreglo[*]}
    return 0
}

function todos {
    echo ${arreglo[*]}    
}

select linea in existe replace eliminar cantidad todos salir 
do
 case $linea in
 existe)
    read -p "ingresar un numero para ver si existe en el arreglo, retorna indice si existe: " num
    existe $num
 ;; 
 replace)
    read -p "ingresar dos numeros, el segundo reemplaza el primero en el arreglo si existe: " num1 num2
    replace $num1 $num2
 ;;
 eliminar)
    read -p "ingresar un numero para eliminar en el arreglo: " num
    eliminar $num
 ;;
 cantidad)
    cantidad
 ;;
 todos)
    todos
 ;;
 salir)
    exit 0
 ;;
 esac
done 

exit 0
