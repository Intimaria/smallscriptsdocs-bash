#!/bin/bash

# creo un nuevo directorio para guardar los
# archivos decomprimidos y tenerlos separados
# para evaluarlos con mayor seguridad y comodidad

mkdir input 
tar -xf input.tar -C input

# esto testea si el comando tar funciono,
# sino, termina el programa con codigo 1

if [ $? -ne 0 ]
then 
	exit 1
fi 

cd input

# esto testea si el comando cd funciono,
# sino, termina el programa con codigo 2

if [ $? -ne 0 ]
then 
	exit 2
fi

# num es el numero de archivos en 
# el directorio input divido por la 
# mitad ya que se que son la misma
# cantidad de archivos

num=$(( $(ls | wc -l) / 2 ))

# creo un arreglo para almacenar las
# diferencias entre archivos, 0 si son
# iguales, 1 si no lo son, y un contador
# para ir sumando la cantidad de archivos
# diferentes

arreglo=()
contador=0

# por el numero de archivos / 2 voy a ver si
# los archivos p y s son diferentes y luego 
# a partir del codigo de retorno del comando diff
# si me devuelve 0, entra en el then, si es 1
# por que no son iguales, entonces entra en
# el else. 

for (( i = 1 ; i <= $num ; i++ ))
do 

  if diff s$i p$i > /dev/null 2>&1
  then 
     arreglo[i]=0
  else 
     arreglo[i]=1
     let contador++
  fi

done
 
# imprimira el arreglo y el contador 

echo ${arreglo[*]}
echo $contador
