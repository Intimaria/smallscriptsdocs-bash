El comando diff es un comando muy util en Unix que nos permite rapida y eficientemente comparar archivos. Es la base de muchas herramientas célebres de la industria de IT :).

Investigue en breves minutos el uso básico del comando y resuelva el siguiente
ejercicio:

Dado un archivo empaquetado (input.tar) con una serie de archivos que provienen de dos servidores(S y P). Nuestra tarea es simple y consiste en comparar los archivos de S y P.

Los archivos que provienen del servidor S se llaman s1, s2......sn
Los archivos que provienen del servidor P se llaman p1, p2......pn

Sabemos que tenemos la misma cantidad de archivos provenientes de S y de P.

Utilizando el comando diff, ud debe comparar todos los archivos provenientes de S y P:
s1 vs p1,
s2 vs p2
.....
sn vs pn

y así para todos los pares de archivos provenientes de S y P.

Su script debe:

1) Desempaquetar input.tar
2) Inicializar un arreglo y en cada posición del arreglo debe registrar el resultado de la comparación de archivos de s y p.

Cada posición i del arreglo tendrá:

0 (cero) si NO hay diferencias entre los archivos si y pi.
1 (uno) si hay diferencias entre los archivos si y pi .

Finalmente debe imprimir en pantalla el arreglo resultante y luego y en otra linea, la cantidad total de diferencias detectadas.

Por ejemplo:

Si el archivo input.tar tiene 3 pares de archivos:

S1,S2,S3
P1,P2,P3

y además:
```
    S1 es igual a P1,

    S2 es DISTINTO a P2

    S3 es igual a P3
```

El resultado será
```
0 1 0
1
```
Explicación: el array resultante tiene un 1 en la seguda posición ya que s2 es distinto que p2. y Además hay una sola diferencia.

Es imperativo y OBLIGATORIO que su script retorne los valores en el orden
indicado y como se detallo anteriormente.

No imprima cosas innecesarias por pantalla. En tal caso el resultado se
considera inválido.
