#!/bin/bash
#two parameters only
if [[ $# -ne 2 ]]
then
	echo "INVALID PARAMETERS"
	exit 1
fi
# parameters are not empty
if [[ -z "$1" ||  -z "$2" ]]
then 
	echo "INVALID PARAMETERS"
	exit 2
fi
# parameters are strings
if ! [[ "$1" =~ ^[a-zA-Z]+$ ||  "$2" =~ ^[a-zA-Z]+$ ]]
then
  echo "INVALID PARAMETERS"
  exit 3
fi
# all codes are 2 chars
x="$1"
if [[ ${#x} -ne 2 ]]
then
	echo "INVALID PARAMETERS"
	exit 4
fi
# save necessary fields
alt=$(cat 100_CC_Records_-_100_CC_Records.csv .csv |  grep $1 | grep "$2" | cut -d, -f1,3,4,11 )
arr=()
suma=0

# read line by line
while read -r i;
do
	var=$(echo $i | cut -d',' -f3)
	arr=( ${arr[@]} "$var" ) # array of card numbers 
	var1=$(echo $i | cut -d',' -f4);
	let "suma=suma + $var1" 2> /dev/null # sum of limits
done <<< "$alt"

# only prints if I have values
if ! [[ -z $arr ]] || [[ $suma -ne 0 ]]; then
echo ${arr[@]}
echo $suma
fi 
exit 0
