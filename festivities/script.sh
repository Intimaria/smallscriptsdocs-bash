#!/bin/bash

if [ -z "$1" ]; then 
	echo "INVALID PARAMETERS"
	exit 1
fi

if [ $2 -le 1 ] || [ $2 -gt $(cat fiestas-y-festivales.csv | wc -l) ]; then
	echo "INVALID PARAMETERS"
	exit 2
fi

PROVINCIA="^$1$"
FILA=$2
FESTIVIDAD=^$3

prov=$(cat fiestas-y-festivales.csv | cut -d"," -f5 | grep "${PROVINCIA}" | wc -l)
echo $prov

head -n $FILA fiestas-y-festivales.csv | tail -1 | cut -d"," -f7 

fest=$(cat fiestas-y-festivales.csv | cut -d"," -f18 | grep -i "${FESTIVIDAD}" | wc -l) 
echo $fest
